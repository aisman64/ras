client:
	gcc ras.c -o ras
server:
	-pkill -9 rasd
	gcc -pthread rasd.c -o rasd
	./rasd

all:
	-pkill -9 rasd
	gcc ras.c -o ras
	gcc -pthread rasd.c -o rasd
	./rasd

git:
	git add rasd.c ras.c
	git commit -m "$m"
	git push origin master
 
