#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <unistd.h>
#include <netdb.h>
#include <errno.h>
#include <syslog.h>
#include <fcntl.h>
#include <stdarg.h>
#include <pthread.h>
#include <sys/socket.h>
#include <sys/wait.h>
#include <sys/resource.h>

#define QLEN 10
#define MAX_BUF 1024
#define MAX_LINE 128
#define MAX_VARS 10
#define MAX_PIPES 10
#define MAX_N 500
#define MAX_FILE 64
#ifndef HOST_NAME_MAX
#define HOST_NAME_MAX 256
#endif

struct cmd {
	char *cmd;
	char *varlist[MAX_VARS];
	int varnum;
	int out;
};

struct pinfo{
	int out;
	char outfile[MAX_FILE];
	struct cmd cmdArr[MAX_PIPES];
	int pipeNum;
};

typedef struct pinfo pInfo;


void err_quit(const char *);
void daemonize(const char *);
int initserver(int, const struct sockaddr *, socklen_t, int);
void serve(int);
void *terminal(void *);
pInfo *parse(int, char *);
void freeInfo(pInfo *);
void printInfo(int, pInfo *);
void initInfo(pInfo *);
void parse_command(char *, struct cmd *);
void command(int, pInfo *);

int main(int argc, char *argv[])
	{
	struct addrinfo *ailist, *aip;
	struct addrinfo hint;
	int sockfd, err, n;
	char *host, *host2;
	if(argc != 1)
		err_quit("usage: rasd\n");
	#ifdef _SC_HOST_NAME_MAX
		n = sysconf(_SC_HOST_NAME_MAX);
		if(n < 0) /* best guess */
	#endif
	n = HOST_NAME_MAX;
	host = malloc(n);
	if(host == NULL)
		err_quit("malloc error\n");
	if(gethostname(host, n) < 0)
		err_quit("gethostname error\n");
	daemonize("rasd");
	hint.ai_flags = AI_PASSIVE;
	hint.ai_family = 0;
	hint.ai_socktype = SOCK_STREAM;
	hint.ai_protocol = 0;
	hint.ai_addrlen = 0;
	hint.ai_canonname = NULL;
	hint.ai_addr = NULL;
	hint.ai_next = NULL;
	if((err = getaddrinfo(NULL, "10101", &hint, &ailist)) != 0)
		{
		syslog(LOG_ERR, "rasd: getaddrinfo error: %s", gai_strerror(err));
		exit(1);
		}
	for(aip = ailist; aip != NULL; aip = aip->ai_next)
		{
		if((sockfd = initserver(SOCK_STREAM, aip->ai_addr, aip->ai_addrlen, QLEN)) >= 0)
			{
			serve(sockfd);
			exit(0);
			}
		}
		exit(1);	
	}


//Function initserver taken from APUE chapter 16, figure 16.20
int initserver(int type, const struct sockaddr *addr, socklen_t alen, int qlen)
	{
      	int fd, err;
	int reuse = 1;
   	if((fd = socket(addr->sa_family, type, 0)) < 0)
        	return(-1);
	if(setsockopt(fd, SOL_SOCKET, SO_REUSEADDR, &reuse, sizeof(int)) < 0)
		{
		err = errno;
		goto errout;
		}
    	if (bind(fd, addr, alen) < 0) 
		{
        	err = errno;
        	goto errout;
    		}
    	if(type == SOCK_STREAM || type == SOCK_SEQPACKET) 
		{
        	if (listen(fd, qlen) < 0) 
			{
            		err = errno;
            		goto errout;
        		}
    		}
    	return(fd);
	errout:
    		close(fd);
    		errno = err;
    		return(-1);
	}


void err_quit(const char* msg)
	{
	fprintf(stderr, "%s", msg);
	abort();
	exit(1);
	}

//Function daemonize taken From Figure 13.1 APUE
void daemonize(const char *cmd)
	{
	int i, fd0, fd1, fd2;
	pid_t pid;
	struct rlimit rl;
	struct sigaction sa;
	//Clear file creation mask
	umask(0);
	//////////////////////////
	//Get max number of file descriptors.
	if(getrlimit(RLIMIT_NOFILE, &rl) < 0)
		err_quit("Can't get file limit... quitting\n");
	//Lose controlling TTY
	if((pid = fork()) < 0)
		err_quit("Can't Fork... quitting\n");
	else if(pid != 0) //parent
		exit(0);
	setsid();
	//ensure future opens wont allocate controlling TTYs.
	sa.sa_handler = SIG_IGN;
	sigemptyset(&sa.sa_mask);
	sa.sa_flags = 0;
	if(sigaction(SIGHUP, &sa, NULL) < 0)
		err_quit("SIGHUP received... quitting\n");
	if((pid = fork()) < 0)
		err_quit("Can't Fork... quitting\n");
	//Close all open fds.
	if(rl.rlim_max == RLIM_INFINITY)
		rl.rlim_max = 1024;
	for(i=0; i<rl.rlim_max; i++)
		close(i);
	//attach fd's 0, 1 and 2 to null
	fd0 = open("/dev/null", O_RDWR);
	fd1 = dup(0);
	fd2 = dup(0);
	//Initialize logfile
	openlog(cmd, LOG_CONS, LOG_DAEMON);
	if(fd0 !=0 || fd1 != 1 || fd2 != 2)
		{
		syslog(LOG_ERR, "unexpected file descriptors %d %d %d", fd0, fd1, fd2);
		exit(1);
		}
	}


//Figure 16.15 APUE
void serve(int sockfd)
	{
	int clfd, status;
	pid_t pid;
	unsigned int ids;
	pthread_attr_t attr;
	pthread_t threads;
	pthread_attr_init(&attr);
	while(1)
		{
		clfd = accept(sockfd, NULL, NULL);
		if(clfd < 0)
			{
			syslog(LOG_ERR, "rasd: accept error: %s", strerror(errno));
			exit(1);
			}
		ids = clfd;
		pthread_create(&threads, &attr, terminal, &ids);
		}
	close(sockfd);
	return;
	}

void *terminal(void * arg)
	{
	setenv("PATH", "bin:.", 1);
	char cwd[1024], data[MAX_BUF];
	unsigned int clfd = *(unsigned int *)arg;
	int dlen = 1;
	pInfo *info;
	if(getcwd(cwd, sizeof(cwd)) == NULL)
		strcpy(cwd, "/");
	dprintf(clfd, "********************************************************************************\n");
	dprintf(clfd, "** Welcome to information server, B00902112                                   **\n");
	dprintf(clfd, "********************************************************************************\n");
	dprintf(clfd, "** You are in directory %s  %23s\n", cwd, "**");
	dprintf(clfd, "** This directory will be under \"/\", in this system.                          **\n");
	dprintf(clfd, "********************************************************************************\n");
	dprintf(clfd, "** The directory bin/ includes:                                               **\n");
	dprintf(clfd, "**                   cat                                                      **\n");	
	dprintf(clfd, "**                   ls                                                       **\n");			
	dprintf(clfd, "**                   grep                                                     **\n");
	dprintf(clfd, "********************************************************************************\n");
	dprintf(clfd, "** In addition, the following two commands are supported by ras.              **\n");
	dprintf(clfd, "**	setenv                                                                **\n");	
	dprintf(clfd, "**	printenv                                                              **\n");
	dprintf(clfd, "********************************************************************************\n");
	while(dlen)
		{
		dlen = recv(clfd, data, MAX_BUF, 0);
		if(dlen)
			{
			info = parse(clfd, data);
			command(clfd, info);			
			}
		}
	close(clfd);
	pthread_exit(NULL);
	}



void initInfo(pInfo *p)
	{
	int i;
	p->out = 0;
	p->pipeNum = 0;
	for(i=0; i<MAX_PIPES; i++)
		{
		p->cmdArr[i].cmd = NULL;
		p->cmdArr[i].varlist[0] = NULL;
		p->cmdArr[i].varnum = 0;
		}
	}

void parse_command(char *cmd, struct cmd *comm)
	{
	int i=0, pos=0;
	char word[MAX_LINE];
	comm->varnum=0;
	comm->cmd = 0;
	comm->varlist[0] = NULL;
	while(isspace(cmd[i]))
		i++;
	if(cmd[i] == '\0')
		return;
	while(cmd[i] != '\0')
		{
		while(cmd[i] != '\0' && !isspace(cmd[i]))
			word[pos++] = cmd[i++];
		word[pos] = '\0';
		comm->varlist[comm->varnum] = malloc((strlen(word)+1)*sizeof(char));
		strcpy(comm->varlist[comm->varnum], word);
		comm->varnum++;
		word[0]='\0';
		pos=0;
		while(isspace(cmd[i]))
			i++;
		}
	comm->cmd = malloc((strlen(comm->varlist[0])+1)*sizeof(char));
	strcpy(comm->cmd, comm->varlist[0]);
	comm->varlist[comm->varnum] = NULL;
	}

pInfo *parse(int clfd, char *line)
	{
	pInfo *result;
	int i=0,j=0, pos, end=0, com_pos;
	char cmd[MAX_LINE];
	char tmp[4];
	if(line[i]=='\n' && line[i]=='\0')
		return NULL;
	result = malloc(sizeof(pInfo));
	if(result == NULL)
		return NULL;
	initInfo(result);
	com_pos = 0;
	while(line[i]!='\n' && line[i]!='\0')
		{
		if(line[i]=='>')
			{
			result->out = 1;
			while(isspace(line[++i]));
			pos = 0;
			while(line[i]!='\0' && !isspace(line[i]))
				{
				if(pos==MAX_FILE)
					{
					dprintf(clfd, "Error, output redirection file exceeds max length.\n");
					freeInfo(result);
					return NULL;
					}
				result->outfile[pos++] = line[i++];	
				}
			result->outfile[pos]='\0';
			end = 1;
			while(isspace(line[i]))
				{
				if(line[i]=='\n')
					break;
				i++;
				}
			}
		else if(line[i]=='|' && !isdigit(line[i+1]))
			{
			cmd[com_pos]='\0';
			parse_command(cmd, &result->cmdArr[result->pipeNum]);
			com_pos = 0;
			end = 0;
			result->pipeNum++;
			i++;
			}
		else if(line[i]=='|' && isdigit(line[i+1]))
			{
			while(isdigit(line[++i]))
				tmp[i] = line[i];
			tmp[i] = '\0';
			cmd[com_pos] = '\0';
			
			com_pos = 0;
			end = 0;
		
			}
		else
			{
			if(end==1)
				{
				dprintf(clfd, "Wrong format or input.\n");
				freeInfo(result);
				return NULL;
				}
			if(com_pos == MAX_LINE-1)
				{
				dprintf(clfd, "Command exceeds the line length limit.\n");
				freeInfo(result);
				return NULL;
				}
			cmd[com_pos++] = line[i++];
			}
		}
	cmd[com_pos] = '\0';
	parse_command(cmd, &result->cmdArr[result->pipeNum]);
	return result;	
	}

void printInfo(int clfd, pInfo *info)
	{
	int i,j;
	struct cmd *comm;
	if(info == NULL)
		{
		dprintf(clfd, "Null Info.\n");
		return;
		}
	dprintf(clfd, "Parse struct:\n\n");
	dprintf(clfd, "# of pipes: %d\n", (info->pipeNum));
	for(i=0; i<=info->pipeNum; i++)
		{
		comm = &(info->cmdArr[i]);
		if((comm==NULL) || (comm->cmd==NULL))
			dprintf(clfd, "Command %d is null.\n", i);
		else
			{
			dprintf(clfd, "Command %d is %s.\t", i+1, comm->cmd);
			for(j=0; j<comm->varnum; j++)
				dprintf(clfd, "Arg %d: %s ", j, comm->varlist[j]);
			dprintf(clfd, "\n");
			}
		}
	dprintf(clfd, "\n");
	if(info->out)
		dprintf(clfd, "outfile: %s\n", info->outfile);
	else
		dprintf(clfd, "no output redirection.\n");
	}

void freeInfo(pInfo *info)
	{
	int i,j;
	struct cmd *comm;
	if(info==NULL)
		return;
	for(i=0; i<MAX_PIPES; i++)
		{
		comm = &(info->cmdArr[i]);
		for(j=0; j<comm->varnum; j++)
			free(comm->varlist[j]);
		if(comm->cmd == NULL)
			free(comm->cmd);
		}
	free(info);
	}


void command(int clfd, pInfo *info)
	{
	int status, i, j=0, k, n=info->pipeNum, c=n+1, p[2*n], l;
	pid_t pid;
	if(strcmp(info->cmdArr[n].cmd, "printenv")==0)
				{
				if(info->cmdArr[n].varlist[1]==NULL)
					dprintf(clfd, "wrong usage of printenv command\n");
				else
					dprintf(clfd, "%s=%s\n",info->cmdArr[n].varlist[1],  getenv(info->cmdArr[n].varlist[1]));
				return;
				}
	else if(strcmp(info->cmdArr[n].cmd, "setenv")==0)
			{
			setenv(info->cmdArr[n].varlist[1], info->cmdArr[n].varlist[2], 1);
			return;
			}
	for(i=0; i<=n; i++)
		{
		for(j=0; j<info->cmdArr[i].varnum; j++)
			{
			if(strchr(info->cmdArr[i].varlist[j], '/')!=NULL)
				{
				dprintf(clfd, "/ is not allowed into arguments as a security measure\n");
				return;
				}
			}
		}
	for(i=0; i<n; i++)
		{
		if(pipe(p+(i*2))<0)
			{			
			syslog(LOG_ERR, "ruptimed: pipe error: %s", strerror(errno));
			exit(1);
			}
		}
	for(i=0; i<=n; ++i)
		{
		if((pid = fork()) < 0)
			{
			syslog(LOG_ERR, "ruptimed: fork error: %s", strerror(errno));
			exit(1);
			}
		else if(pid == 0) //child
			{
			if(i==n) //if last command
				{
				if(info->out == 1)
					{
					FILE *fp = fopen(info->outfile, "w+");
					int fd = fileno(fp);
					if(dup2(fd, STDOUT_FILENO) != STDOUT_FILENO || dup2(fd, STDERR_FILENO) != STDERR_FILENO)
						{
						syslog(LOG_ERR, "ruptimed: unexpected error");
						exit(1);
						}
					close(fd);
					fclose(fp);
					}
				else
					{
					if(dup2(clfd, STDOUT_FILENO) != STDOUT_FILENO || dup2(clfd, STDERR_FILENO) != STDERR_FILENO)
						{
						syslog(LOG_ERR, "ruptimed: unexpected error");
						exit(1);
						}
					close(clfd);
					}
				}
			if(i<n) //if not last command
				{
				if(dup2(p[j+1], STDOUT_FILENO) != STDOUT_FILENO || dup2(p[j+1], STDERR_FILENO) != STDERR_FILENO)
					{
					syslog(LOG_ERR, "ruptimed: dup2 error");
					exit(1);
					}
				}
			if(j!=0) //if not first command
				{
				if(dup2(p[j-2], STDIN_FILENO) != STDIN_FILENO)
					{
					syslog(LOG_ERR, "ruptimed: unexpected error");
					exit(1);
					}
				}
			for(k=0; k<2*n; k++)
				close(p[k]);
			if(strcmp(info->cmdArr[i].cmd, "ls")==0)
				execv("bin/ls", info->cmdArr[i].varlist);
			else if(strcmp(info->cmdArr[i].cmd, "cat")==0)
				execv("bin/cat", info->cmdArr[i].varlist);
			else if(strcmp(info->cmdArr[i].cmd, "grep")==0)
				execv("bin/grep", info->cmdArr[i].varlist);
			}
		j+=2;
		}
	for(i=0; i<2*n; i++)
		close(p[i]);
	for(i=0; i<n+1; i++)
		wait(&status);
	}

