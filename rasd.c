#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <netdb.h>
#include <errno.h>
#include <syslog.h>
#include <fcntl.h>
#include <stdarg.h>
#include <pthread.h>
#include <sys/socket.h>
#include <sys/wait.h>
#include <sys/resource.h>

#define QLEN 10
#ifndef HOST_NAME_MAX
#define HOST_NAME_MAX 256
#endif


void err_quit(const char* msg);
void daemonize(const char *cmd);
int initserver(int type, const struct sockaddr *addr, socklen_t alen, int qlen);
void serve(int sockfd);
void *terminal(void * arg);
void uptime(int clfd);

int main(int argc, char *argv[])
	{
	struct addrinfo *ailist, *aip;
	struct addrinfo hint;
	int sockfd, err, n;
	char *host, *host2;
	if(argc != 1)
		err_quit("usage: rasd\n");
	#ifdef _SC_HOST_NAME_MAX
		n = sysconf(_SC_HOST_NAME_MAX);
		if(n < 0) /* best guess */
	#endif
	n = HOST_NAME_MAX;
	host = malloc(n);
	if(host == NULL)
		err_quit("malloc error\n");
	if(gethostname(host, n) < 0)
		err_quit("gethostname error\n");
	daemonize("rasd");
	hint.ai_flags = AI_CANONNAME | AI_PASSIVE;
	hint.ai_family = 0;
	hint.ai_socktype = SOCK_STREAM;
	hint.ai_protocol = 0;
	hint.ai_addrlen = 0;
	hint.ai_canonname = NULL;
	hint.ai_addr = NULL;
	hint.ai_next = NULL;
	if((err = getaddrinfo(host, "10101", &hint, &ailist)) != 0)
		{
		syslog(LOG_ERR, "rasd: getaddrinfo error: %s", gai_strerror(err));
		exit(1);
		}
	for(aip = ailist; aip != NULL; aip = aip->ai_next)
		{
		if((sockfd = initserver(SOCK_STREAM, aip->ai_addr, aip->ai_addrlen, QLEN)) >= 0)
			{
			serve(sockfd);
			exit(0);
			}
		}
		exit(1);	
	}


//Function initserver taken from APUE chapter 16, figure 16.20
int initserver(int type, const struct sockaddr *addr, socklen_t alen, int qlen)
	{
      	int fd, err;
	int reuse = 1;
   	if((fd = socket(addr->sa_family, type, 0)) < 0)
        	return(-1);
	if(setsockopt(fd, SOL_SOCKET, SO_REUSEADDR, &reuse, sizeof(int)) < 0)
		{
		err = errno;
		goto errout;
		}
    	if (bind(fd, addr, alen) < 0) 
		{
        	err = errno;
        	goto errout;
    		}
    	if(type == SOCK_STREAM || type == SOCK_SEQPACKET) 
		{
        	if (listen(fd, qlen) < 0) 
			{
            		err = errno;
            		goto errout;
        		}
    		}
    	return(fd);
	errout:
    		close(fd);
    		errno = err;
    		return(-1);
	}


void err_quit(const char* msg)
	{
	fprintf(stderr, "%s", msg);
	abort();
	exit(1);
	}

//Function daemonize taken From Figure 13.1 APUE
void daemonize(const char *cmd)
	{
	int i, fd0, fd1, fd2;
	pid_t pid;
	struct rlimit rl;
	struct sigaction sa;
	//Clear file creation mask
	umask(0);
	//////////////////////////
	//Get max number of file descriptors.
	if(getrlimit(RLIMIT_NOFILE, &rl) < 0)
		err_quit("Can't get file limit... quitting\n");
	//Lose controlling TTY
	if((pid = fork()) < 0)
		err_quit("Can't Fork... quitting\n");
	else if(pid != 0) //parent
		exit(0);
	setsid();
	//ensure future opens wont allocate controlling TTYs.
	sa.sa_handler = SIG_IGN;
	sigemptyset(&sa.sa_mask);
	sa.sa_flags = 0;
	if(sigaction(SIGHUP, &sa, NULL) < 0)
		err_quit("SIGHUP received... quitting\n");
	if((pid = fork()) < 0)
		err_quit("Can't Fork... quitting\n");
	//Close all open fds.
	if(rl.rlim_max == RLIM_INFINITY)
		rl.rlim_max = 1024;
	for(i=0; i<rl.rlim_max; i++)
		close(i);
	//attach fd's 0, 1 and 2 to null
	fd0 = open("/dev/null", O_RDWR);
	fd1 = dup(0);
	fd2 = dup(0);
	//Initialize logfile
	openlog(cmd, LOG_CONS, LOG_DAEMON);
	if(fd0 !=0 || fd1 != 1 || fd2 != 2)
		{
		syslog(LOG_ERR, "unexpected file descriptors %d %d %d", fd0, fd1, fd2);
		exit(1);
		}
	}


//Figure 16.15 APUE
void serve(int sockfd)
	{
	int clfd, status;
	pid_t pid;
	unsigned int ids;
	pthread_attr_t attr;
	pthread_t threads;
	pthread_attr_init(&attr);
	while(1)
		{
		clfd = accept(sockfd, NULL, NULL);
		if(clfd < 0)
			{
			syslog(LOG_ERR, "rasd: accept error: %s", strerror(errno));
			exit(1);
			}
		ids = clfd;
		pthread_create(&threads, &attr, terminal, &ids);
		}
	close(sockfd);
	return;
	}

void *terminal(void * arg)
	{
	char cwd[1024], data[1024];
	unsigned int clfd = *(unsigned int *)arg;
	int dlen = 1;
	if(getcwd(cwd, sizeof(cwd)) == NULL)
		strcpy(cwd, "/");
	dprintf(clfd, "********************************************************************************\n");
	dprintf(clfd, "** Welcome to information server, B00902112                                   **\n");
	dprintf(clfd, "********************************************************************************\n");
	dprintf(clfd, "** You are in directory %s", cwd);
	dprintf(clfd, "%29s\n", "**");
	dprintf(clfd, "** This directory will be under \"/\", in this system.                          **\n");
	dprintf(clfd, "********************************************************************************\n");
	dprintf(clfd, "** The directory bin/ includes:                                               **\n");
	dprintf(clfd, "**                   cat                                                      **\n");	
	dprintf(clfd, "**                   ls                                                       **\n");			
	dprintf(clfd, "**                   grep                                                     **\n");
	dprintf(clfd, "********************************************************************************\n");
	dprintf(clfd, "** In addition, the following two commands are supported by ras.              **\n");
	dprintf(clfd, "**	setenv                                                                **\n");	
	dprintf(clfd, "**	printenv                                                              **\n");
	dprintf(clfd, "********************************************************************************\n");
	while(dlen)
		{
		dlen = recv(clfd, data, 1024, 0);
		if(dlen)
			{
			send(clfd, data, dlen, 0);
			}
		}
	close(clfd);
	pthread_exit(NULL);
	}

void ls(int clfd, char* arg)
	{
	int status;
	pid_t pid;
	if((pid = fork()) < 0)
		{
		syslog(LOG_ERR, "ruptimed: fork error: %s", strerror(errno));
		exit(1);
		}
	else if(pid == 0) //child
		{
		if(dup2(clfd, STDOUT_FILENO) != STDOUT_FILENO || dup2(clfd, STDERR_FILENO) != STDERR_FILENO)
			{
			syslog(LOG_ERR, "ruptimed: unexpected error");
			exit(1);
			}
		close(clfd);
		execl("/usr/bin/ls", "ls", arg, (char *)0);
		syslog(LOG_ERR, "ruptimed: unexpected return from exec: %s", strerror(errno));
		}
	else
		{
		waitpid(pid, &status, 0);
		}		
	}

void cat(int clfd, char* arg)
	{
	int status;
	pid_t pid;
	if((pid = fork()) < 0)
		{
		syslog(LOG_ERR, "ruptimed: fork error: %s", strerror(errno));
		exit(1);
		}
	else if(pid == 0) //child
		{
		if(dup2(clfd, STDOUT_FILENO) != STDOUT_FILENO || dup2(clfd, STDERR_FILENO) != STDERR_FILENO)
			{
			syslog(LOG_ERR, "ruptimed: unexpected error");
			exit(1);
			}
		close(clfd);
		execl("/usr/bin/cat", "cat", arg, (char *)0);
		syslog(LOG_ERR, "ruptimed: unexpected return from exec: %s", strerror(errno));
		}
	else
		{
		waitpid(pid, &status, 0);
		}		
	}

void grep(int clfd, char* arg)
	{
	int status;
	pid_t pid;
	if((pid = fork()) < 0)
		{
		syslog(LOG_ERR, "ruptimed: fork error: %s", strerror(errno));
		exit(1);
		}
	else if(pid == 0) //child
		{
		if(dup2(clfd, STDOUT_FILENO) != STDOUT_FILENO || dup2(clfd, STDERR_FILENO) != STDERR_FILENO)
			{
			syslog(LOG_ERR, "ruptimed: unexpected error");
			exit(1);
			}
		close(clfd);
		execl("/usr/bin/grep", "grep", arg, (char *)0);
		syslog(LOG_ERR, "ruptimed: unexpected return from exec: %s", strerror(errno));
		}
	else
		{
		waitpid(pid, &status, 0);
		}		
	}
