#include <stdio.h>
#include <stdlib.h>
#include <netdb.h>
#include <errno.h>
#include <string.h>
#include <unistd.h>
#include <sys/socket.h>

#define MAXADDRLEN 256
#define BUFLEN 128
#define MAXSLEEP 128

void err_quit(const char* msg);
void print_answer(int sockfd);
int connect_retry(int sockfd, const struct sockaddr *addr, socklen_t alen);

int main(int argc, char *argv[])
	{
	struct addrinfo *ailist, *aip;
	struct addrinfo hint;
	int sockfd, err;
	fprintf(stderr, "%s\n", argv[1]); 
	if(argc != 2)
		err_quit("usage: ras hostname\n");
	hint.ai_flags = 0;
	hint.ai_family = 0;
	hint.ai_socktype = SOCK_STREAM;
	hint.ai_protocol = 0;
	hint.ai_addrlen = 0;
	hint.ai_canonname = NULL;
	hint.ai_addr = NULL;
	hint.ai_next = NULL;
	if((err = getaddrinfo(argv[1], "10101", &hint, &ailist)) != 0)
		err_quit("getaddrinfo error\n");
	for(aip = ailist; aip != NULL; aip = aip->ai_next)
		{
		if((sockfd = socket(aip->ai_family, SOCK_STREAM, 0)) < 0)
			err = errno;
		if(connect_retry(sockfd, aip->ai_addr, aip->ai_addrlen) < 0)
			err = errno;
		else
			{
			print_answer(sockfd);
			exit(0);
			}
		}
	fprintf(stderr, "can't connect to %s: %s\n", argv[1], strerror(err));
	exit(1);
	}

int connect_retry(int sockfd, const struct sockaddr *addr, socklen_t alen)
	{
	int nsec;
	//Try connecting with exp backoff
	for(nsec=1; nsec<=MAXSLEEP; nsec<<=1)
		{
		if(connect(sockfd, addr, alen) == 0)
			return 0;
		if(nsec<= MAXSLEEP/2)
			sleep(nsec);
		}
	return -1;
	}

void print_answer(int sockfd)
	{
	int n;
	char buf[BUFLEN];
	while((n = recv(sockfd, buf, BUFLEN, 0)) > 0)
		write(STDOUT_FILENO, buf, n);
	if(n<0)
		{
		fprintf(stderr, "errno: %s\n", strerror(errno));
		err_quit("recv error\n");
		}
	}

void err_quit(const char* msg)
	{
	fprintf(stderr, "%s", msg);
	abort();
	exit(1);
	}
